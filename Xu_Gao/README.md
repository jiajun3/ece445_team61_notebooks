# Xu Gao Worklog

[[_TOC_]]

# 2023-01-29- Discussion with group members

We came up with a few project ideas that could be our project topic potentially. They were bee-watcher, glove taker, adjustable phone holder, etc. We had problem to make the final decision. We decided to come to the TAs office hour to seek for advice. 

# 2023-02-07- First TA meeting

We talked about the primary design of our project, which was an automated wildlife watcher. This topic had been approved, but Zicheng mentioned some noticable features of our design - machine learning for system to recognize different animals, testing place for demo. The design was ambitious, but we might not be able to finish it in one semester. We were suggested to remove some features to simplify our design.

# 2023-02-18- Parts Updates

Our consideration regarding the control system was not clear enough. To make sure my job was on the right track, I chose to design the power subsystem at first. The original idea was to use a flyback converter as the voltage converter that converted 12V input voltage to 5V (supposed) output voltage.

![](power1.png)

However, with more consideration about cost and efficiency, I decided to use a designed dc/dc converter chip named R-78E5.0 which can convert the input voltage to 5V with a switching regulator. The reason why the original plan was abandoned was because there is no need for a very high input voltage for our project. Moreover, with the flyback converter, I had to design an extra voltage regulator to connect with it. In this way, I designed the schematic of the power section of our PCB.

![](power2.png)

# 2023-02-24- Parts Updates

The microcontroller submodule in our design was not clear enough. The primary idea was to use Raspberry Pi to control the USB camera. There was a problem in this way - how to build the connection between microcontroller and the gimbal submodule. The servo motors in gimbal submodule required two PWM signals generated from the microcontroller. Currently, I am looking at how to control gimbal. Here is one example I found that is close to our design.

![](pic1.png)

[link](https://howtomechatronics.com/projects/diy-arduino-gimbal-self-stabilizing-platform/)

This one takes an Arduino board as its MCU. The type of motors used in this project is also similar to our design (MG995). I try to design our PCB based on this project idea. I am sure that this will not be our final PCB, because it is highly possible we cannot use two common MCU boards. I am confused about how to setup the connection between camera control and gimbal control.

![](pic2.png)

# 2023-03-01- Design Review

From the meeting with Professor Mironenko, we were informed that we could not use a single Raspberry Pi as our microcontroller since it is required to design our own PCB. Our understanding towards the design of microcontrolller module is not clear enough. I have to figure out a type of MCU we will use in our design. A basic idea in my mind is to use a MCU to control the gimbal motors and connect it with Raspberry Pi.

# 2023-03-09- Contact with head TA Jason

We have sent email to Jason and Professor Fliflet to ask for some advices about choosing MCU to control both the camera and the gimbal. Jason suggested us to think about ESP32 which can perform object tracking and command the servos in lieu of a Raspberry Pi. Here is an example project that performed object tracking with ESP32:

![](pic3.png)

[link](https://diyodemag.com/projects/object_tracking_with_an_esp32-cam)

To generate the PWM signals to control the servos, Jason suggested me to search for some open-source libraries that use the ESP32's on-chip peripherals. However, there is a problem if we take this advice - we have already ordered our camera module which was an Arducam with USB connection to Raspberry Pi. It seems like we cannot use ESP32 in our design. We cannot abandon the camera we have bought. Another meeting needs to be scheduled with Jason to talk about using another type of MCU to control the servos.

# 2023-03-24- Meeting with head TA Jason

We explained our situation to Jason. He suggested us to make our PCB as a Raspberry Pi hat. The main function for our PCB can be designed to control the servo and communicate with Raspberry Pi. The MCU recommended is ATMega328P. For me, I need to find a template of Raspberry Pi hat in KiCad. The primary design for the connection between Raspberry Pi and ATMega328P is to use SPI protocol or UART protocol. Moreover, I have to think about the some other functions that can be performed by our designed PCB (ATMega328P). There should be other components other than ATMega328P itself.

# 2023-03-30- PCB Updates

With the help of Jason's advice, I designed the first version of our PCB. Our project mainly focused on the software design, so the hardware PCB is not to complicated. It is possible that I have to add more components to our PCB. I decide to use SPI protocol to setup the communication connection between Raspberry Pi and ATMega328P. Because there was error in the template file that I found for Raspberry Pi hat, I decide to make our PCB as a seperate chip from Raspberry Pi. 

![](pcb1.png)

# 2023-04-19- Updates

I finished soldering our PCB. A problem was we did not have 5-pin connectors. We have to buy it ASAP. Then we can start to test our whole system after my teammates finish writing the code.

# 2023-04-25- Final test

We met a problem when we tried to program our ATMega328P on the PCB by an Arduino board. There is a mistake on the PCB design. To program the ATMega328P, it requires an external clock signal. A basic design is to connect XTAL1 and XTAL2 pins on ATMega328P with two 22pF capacitors and one 16MHz crystal oscillator. However, there is not enough time for us to make a new PCB. To make our final product functional, we decide to replace the PCB by Arduino Uno, since they have the similar structure - the MCU on Arduino Uno is an ATMega328.

With enough tests, we decide to use UART protocol to connect our Arduino with Raspberry Pi - connect TX/RX to each board. Since GPIO pins on Raspberry Pi can only support 3.3V voltage, we build a voltage divider on the breadboard in order to protect the RX pin on Raspberry Pi. This circuit will convert 5V from the TX pin on Arduino to 3.3V which is in the safe range for RX pin on Rapsberry Pi. This is a temporary solution to our design:

![](pic4.png)

# 2023-04-29- Revision on PCB

I revised our PCB schematic so that it can deal with the problems we found in the test. In the revised PCB schematic, I added a 6-pin ISP connector to the microcontroller in order to program ATmega328P-U by USBasp. I also connected a 16MHz crystal oscillator and two 22pF capacitors to the microcontroller to serve as an external clock. These make up a minimal USBasp programmable ATMega328P-U circuit. In order to stabilize the output voltage from the voltage divider, I replaced the voltage divider built on the breadboard with a Low-Dropout Linear Regulator (LM1117) which can take 5V as input voltage and provide 3.3V as output voltage.

![](pcb2.png)
