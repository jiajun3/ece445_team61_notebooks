## Automated Wildlife Watcher

A wildlife camera that can keep a certain area of interest not only for a sustained period of time but also can track and follow any wildlife and zoom in for a more clear image or video of their behavior.

## Result
![](result.jpg)
