# Edwin Lu Worklog

[[_TOC_]]

# 2023-01-29- Discussion with group members

We came up with a few project ideas that could be our project topic potentially. They were bee-watcher, glove taker, adjustable phone holder, etc. We had problem to make the final decision. We decided to come to the TAs office hour to seek for advice. 

# 2023-02-07- First TA meeting

We talked about the primary design of our project, which was an automated wildlife watcher. This topic had been approved, but Zicheng mentioned some noticable features of our design - machine learning for system to recognize different animals, testing place for demo. The design was ambitious, but we might not be able to finish it in one semester. We were suggested to remove some features to simplify our design.

# 2023-02-14 - Camera Module Selection and Testing
Today, I researched different camera modules to find one suitable for our project. I considered factors such as image quality, frame rate, and compatibility with our microcontroller.

**Camera Module Selection:**
- Model: [Arducam 1080P Day & Night Vision USB Camera for Computer]
- Specifications: [Image Resolution: 1920*1080, Frame Rate: 30fps, Interface: USB2.0]

**Testing:**
I tested the camera module by connecting it to the microcontroller and capturing images and videos. The image quality and frame rate were satisfactory for our project requirements.

![](camera.jpg)

# 2023-02-25 - Gimbal Module Research
I researched different gimbal modules to stabilize the camera and enable smooth tracking of wildlife. I considered factors such as size, weight, and compatibility with our camera module.

# 2023-03-04 - Camera Module Programming
I researched differnt motion tracking algorithms and downloaded necessary packages for the algorithms on the Raspberry Pi, which is used for image/video processing. And I implemented a tracking algorithm to output the moving object's coordinates.

![](m1.jpg)

# 2023-03-12 - Camera Module Testing
I tested the motion tracking program, it successfully returns the coordinates of the biggest object. And I also managed to decrease the noise on the fram it captures.

# 2023-03-19 - Gimbal Module Programming
I wrote a gimbal control program that takes the desired coordinates as inputs and send signals to move the servos to keep the coordinate in center of the frame. I am using my Arduino Uno as the temporary microcontroller since the PCB is not ready yet.

![](Gimbal_Flowchart.png)

# 2023-03-27 - Gimbal Module Testing
I tested the gimbal control by sending the program a fixed coordinate. It turns out the servos move too fast, thus influencing the recoreded video. I applied the maximum movement value and a smoothing algorithm to solve the issue.

# 2023-04-08 - System Integration
I integrated the camera module, gimbal module, and microcontroller into a single system. And I added serveral "check" signals to make the whole system cooperate better. I tested the complete system by tracking moving objects in a controlled environment.

**Results:**
The system successfully detected and tracked moving objects, with smooth gimbal movement and stable camera output. However, because of the smoothing algorithm, there was a delay when the last command sent to servos are not finished.

Modified Camera Module Flowchart:
![](Camera_Flowchart.png)

# 2023-04-18 - Alternative Microcontroller
PCB is ready now. I tried to use Arduino to bootload the ATMega328p on the PCB with an internal clock. However, the bootloader was not functonal, and it warned me the clock error. So we decided to use Arduino Uno as the microcontroller instead, considering that the ATMega328p we used on PCB is the same as the ATMega328p on Arduino Uno.

# 2023-04-23 - Code Integration and Modification
I changed the code to use Serial Connection between Raspberry Pi and Arduino Uno, and make serveral modificaitons to the communication codes. I wrote a Raspberry Pi desktop program, which is a executable file to avoid showing the codes during operation, making the project more user-friendly.

![](Overall_State_Machine.png)

# 2023-04-25 - Final Testing
I performed final testing on the complete system to evaluate its overall performance, including object detection, tracking, gimbal movement, and system stability.

**Results:**
The automated wildlife tracker performed well in detecting and tracking moving objects. The camera followed the moving object with smooth gimbal movement and stable camera output, except that there is still delay between different movement operations.
